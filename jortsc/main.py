#!/usr/bin/python3

import sys
import pprint
import logging

from jortsc.parser.lexer import lex_jorts
from jortsc.parser.syntatic import syntatic

logging.basicConfig(level=logging.DEBUG)

def main():
    """main entry point"""
    try:
        in_data = sys.stdin.read()
    except EOFError:
        pass

    tokens = lex_jorts(in_data)
    pprint.pprint(tokens)

    tree = syntatic(tokens)
    print(tree)


if __name__ == '__main__':
    main()
