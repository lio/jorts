from dataclasses import dataclass

@dataclass
class TypedVar:
    type_: str
    name: str


@dataclass
class ReturnType:
    type_: str


@dataclass
class Function:
    name: str
    arguments: str
    ret_type: ReturnType
    block: list


@dataclass
class Identifier:
    name: str


@dataclass
class Import:
    module: str


@dataclass
class String:
    value: str


@dataclass
class Number:
    value: str


@dataclass
class FunctionCall:
    function: str
    args: list
