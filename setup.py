from setuptools import setup

setup(
    name='jortsc',
    version='0.1',
    py_modules=['jortsc'],
    install_requires=[
        'lark-parser==0.6.7'
    ],
    entry_points='''
    [console_scripts]
    jortsc=jortsc:main
    '''
)
